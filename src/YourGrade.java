import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int score ;
        String grade;
        System.out.print("Please input your score : ");
        score = sc.nextInt();
        if (score >= 80 && score <= 100){
            System.out.println("Your Grade is A");
        }else if (score < 80 && score >= 75){
            System.out.println("Your Grade is B+");
        }else if (score < 75 && score >= 70){
            System.out.println("Your Grade is B");
        }else if (score < 70 && score >= 65){
            System.out.println("Your Grade is C+");
        }else if (score < 65 && score >= 60){
            System.out.println("Your Grade is C");
        }else if (score < 60 && score >= 55){
            System.out.println("Your Grade is D+");
        }else if (score < 55 && score >= 50){
            System.out.println("Your Grade is D");
        }else{
            System.out.println("Your Grade is F");
        }  
    }
}
